package com.iwsbrazil.futurefaces.poctimewear;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.wearable.DataApi;
import com.google.android.gms.wearable.PutDataMapRequest;
import com.google.android.gms.wearable.PutDataRequest;
import com.google.android.gms.wearable.Wearable;

import static com.iwsbrazil.futurefaces.poctimewear.MobileWearableService.RANDOM_STRING_RCV_INTENT;

public class BluetoothTestActivity extends AppCompatActivity implements
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {

    private static final String TAG = BluetoothTestActivity.class.getSimpleName();
    public static final String RANDOM_STRING_KEY = "com.iwsbrazil.futurefaces.poctimewear.key.randomstring";
    public static final String RANDOM_STRING_PATH = "/random-string";

    private TextView dataToSend;
    private TextView dataReceived;

    private GoogleApiClient googleApiClient;
    private RandomString randomString;
    private String currRandomString;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bluetooth_test);
        dataToSend = (TextView) findViewById(R.id.data_to_send);
        dataReceived = (TextView) findViewById(R.id.data_received);

        googleApiClient = new GoogleApiClient.Builder(this)
                // Request access only to the Wearable API
                .addApiIfAvailable(Wearable.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();

        randomString = new RandomString(10);
        currRandomString = randomString.nextString();

        dataToSend.setText(getString(R.string.data_to_send, currRandomString));
        dataReceived.setText(getString(R.string.data_received, ""));

        LocalBroadcastManager.getInstance(this).registerReceiver(
                dataApiReceiver, new IntentFilter(RANDOM_STRING_RCV_INTENT));
    }

    public void sendData(View view) {
        PutDataMapRequest putDataMapRequest = PutDataMapRequest.create(RANDOM_STRING_PATH);
        putDataMapRequest.getDataMap().putString(RANDOM_STRING_KEY, currRandomString);
        PutDataRequest putDataRequest = putDataMapRequest.asPutDataRequest();
        PendingResult<DataApi.DataItemResult> pendingResult =
                Wearable.DataApi.putDataItem(googleApiClient, putDataRequest);

        pendingResult.setResultCallback(new ResultCallback<DataApi.DataItemResult>() {
            @Override
            public void onResult(@NonNull DataApi.DataItemResult result) {
                if (result.getStatus().isSuccess()) {
                    Log.d(TAG, "Data item set: " + result.getDataItem().getUri());
                }
            }
        });

        /* Updates field with next random string */
        currRandomString = randomString.nextString();
        dataToSend.setText(getString(R.string.data_to_send, currRandomString));
    }

    private BroadcastReceiver dataApiReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String randomString = intent.getStringExtra(RANDOM_STRING_KEY);
            dataReceived.setText(getString(R.string.data_received, randomString));
        }
    };

    @Override
    protected void onStart() {
        super.onStart();
        googleApiClient.connect();
    }

    @Override
    protected void onStop() {
        if (null != googleApiClient && googleApiClient.isConnected()) {
            googleApiClient.disconnect();
        }
        super.onStop();
    }

    @Override
    public void onConnected(@Nullable Bundle connectionHint) {
        Log.d(TAG, "onConnected: " + connectionHint);
    }

    @Override
    public void onConnectionSuspended(int cause) {
        Log.d(TAG, "onConnectionSuspended: " + cause);
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult result) {
        Log.d(TAG, "onConnectionFailed: " + result);
    }
}
