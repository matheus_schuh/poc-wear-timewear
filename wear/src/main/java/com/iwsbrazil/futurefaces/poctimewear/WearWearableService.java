package com.iwsbrazil.futurefaces.poctimewear;

import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.data.FreezableUtils;
import com.google.android.gms.wearable.DataEvent;
import com.google.android.gms.wearable.DataEventBuffer;
import com.google.android.gms.wearable.DataMap;
import com.google.android.gms.wearable.DataMapItem;
import com.google.android.gms.wearable.PutDataMapRequest;
import com.google.android.gms.wearable.Wearable;
import com.google.android.gms.wearable.WearableListenerService;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import static com.iwsbrazil.futurefaces.poctimewear.BluetoothTestActivity.RANDOM_STRING_KEY;
import static com.iwsbrazil.futurefaces.poctimewear.BluetoothTestActivity.RANDOM_STRING_PATH;

public class WearWearableService extends WearableListenerService {

    private static final String TAG = WearWearableService.class.getSimpleName();
    public static final String RANDOM_STRING_RCV_INTENT = "randomStringRcv";

    @Override
    public void onDataChanged(DataEventBuffer dataEvents) {
        Log.d(TAG, "onDataChanged: " + dataEvents);
        final ArrayList<DataEvent> events = FreezableUtils.freezeIterable(dataEvents);

        GoogleApiClient googleApiClient = new GoogleApiClient.Builder(this)
                .addApi(Wearable.API)
                .build();

        ConnectionResult connectionResult =
                googleApiClient.blockingConnect(30, TimeUnit.SECONDS);

        if (!connectionResult.isSuccess()) {
            Log.e(TAG, "Failed to connect to GoogleApiClient.");
            return;
        }

        for (DataEvent event : events) {
            PutDataMapRequest putDataMapRequest =
                    PutDataMapRequest.createFromDataMapItem(DataMapItem.fromDataItem(event.getDataItem()));

            String path = event.getDataItem().getUri().getPath();
            if (event.getType() == DataEvent.TYPE_CHANGED) {
                if (RANDOM_STRING_PATH.equals(path)) {
                    DataMap data = putDataMapRequest.getDataMap();
                    String stringData = data.getString(RANDOM_STRING_KEY);

                    Intent intent = new Intent(RANDOM_STRING_RCV_INTENT);
                    intent.putExtra(RANDOM_STRING_KEY, stringData);
                    LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
                }
            } else if (event.getType() == DataEvent.TYPE_DELETED) {
                /* should not happen */
            }
        }
    }
}